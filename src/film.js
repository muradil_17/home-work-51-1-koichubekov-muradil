import React from 'react';
import './App.css';


const Film = (props) => {

    return (
        <div className="film">
            <img src={props.img} alt="Hackers"/>
            <h1 id="name">{props.name}</h1>
            <p>год: {props.year}</p>
            <button id="watch">Смотреть</button>
        </div>
    );
};

export default Film;