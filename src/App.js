import React from 'react';

import Film from "./film";

import './App.css';
import hack  from  "../src/images/whoami.png"
import fun from "../src/images/1+1.png"
import marvel from "../src/images/avengers.png"


    function App() {
        return (
            <div className="App">
                <Film name="Кто я" img={hack} year="2014"/>
                <Film name="1+1" img={fun} year="2011"/>
                <Film name="Мстители финал" img={marvel}  year="2019"/>
            </div>
        );
    }

export default App;
